function conexionRespForm() {   
  var respForm = SpreadsheetApp.getActive().getSheetByName('Respuestas de formulario 1');  
   return respForm; }

function enviaQR() {
  var lock = LockService.getScriptLock();  
  var respForm = conexionRespForm();
  var datosFormResp = respForm.getDataRange().getValues();
 
  for(var i = 1; i < respForm.getDataRange().getValues().length; i++){
    var enviado = datosFormResp[i][7];
    var imagen = [];
   
      if(enviado != "Enviado"){
        var startRow = 1 + i;
        var hash = datosFormResp[i][2].slice(0,2)+datosFormResp[i][3].slice(0,2)+datosFormResp[i][4].slice(0,2)+i;
        Logger.log(hash);
        var correo = datosFormResp[i][1];
        var nombre = datosFormResp[i][2];
        var cuerpo = 'Bienvenido <b>'+nombre+'</b> al curso <b>Derecho Constitucional</b> impartido por la profesora <b>Fabiola Lara</b> en este correo encontraras un código qr, el cual deberás presentar para tomar asistencia.<br><br> '+
          'Puedes presentar el QR digital o impreso.'+
          '<center><img src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl='+hash+'"><br>Código: <b>'+hash+'</b></center>';
        respForm.getRange(startRow,7).setValue(hash);
       
        GmailApp.sendEmail(correo, "Asistencia Derecho Constitucional", "",{
          htmlBody: cuerpo
        });
        respForm.getRange(startRow,8).setValue('Enviado');
      }
  }
 
 
  lock.releaseLock();
}//fin funcion