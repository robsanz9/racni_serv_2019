<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Bienvenido al Registro</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> <!--Estilo de la pagina con los datos centrados, 
sde CSS --->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body >
	<header style="text-align: center;">    <!--Alineamos el texto de una forma central para que se vea un orden y una estetica-->
          <div class="form">
          <form action="RegistroEvento.php" method="GET">  <!--Creamos un formulario apartir de esta linea-->
                
		<p>Nombre del evento : <input type="text" name="nombre_evento" placeholder="Nombre del evento" required=""></p> 
  	  <!--Campo de texto para especificar el evento que vamos a crear-->
  			<span> Tipo de evento </span>                                        
  		<select id="select" name="tipo_de_evento" required="">                           <!--Boton de listado de opciones-->
  			<option> Conferencia</option>
  			<option> Taller</option>
  			<option> Curso</option>
  			<option> Mesa redonda</option>
  			<option> Congreso</option>
        <option>Otro</option>
  		 </select><br><br>
  		 
  		 	<span> Fecha de inicio:</span>
  			<input type="date" id="date"name="fecha_inicio" placeholder="Fecha de inicio" required=""><br><br>  <!--Tenemos un calendario para seleccionar fecha de inicio-->
        <span>Fecha de término:</span>
        <input type="date"id="date" name="fecha_termino"><br><br>
        <span>Tipo de sede</span>
          <select id="select" name="tipo_de_sede" required="">                   <!--Boton que despliega las sedes disponibles-->
          <option>Nacional</option>
          <option>Internacional</option>
          </select><br><br>
          <span>Lugar</span>
          <select id="select" name="lugar" required="">           <!--Boton que despliega el lugar del evento-->

             <option>Sala de Videoconferencias UIM II</option>
            <option>Aula Magna UIM II</option>
            <option>Unidad de COngresos UIM II</option>
            <option>Otro</option>
          </select> <br><br>

          <span>Tipo de participación</span>
          <select id="select" name="tipo_de_participacion" required="">
            <option>Organizador</option>
            <option>Colaborador</option>
            <option>Invitado</option>
            <option>Otro</option>
          </select>
          <br>
          <p>Descripción : <input type="text" name="descripción" placeholder="Escribe aqui"></p>
          <p>Cupo:<input type="number" name="cupo" placeholder="Escribe aqui"> </p>
          <p>Numero de sesiones:<input type="number" name="numero_de_sesiones"> </p>
          <p>Nombre del ponente:<input type="text" name="nombre_ponente" placeholder="Nombre" required=""></p>
          <span> Número de registros minimos: </span>
          <input type="Number" id="numero " name="registros_minimos"/><br><br>
          <span> Número de regostros máximos: </span>
          <input type="Number" id="numero " name="registros_maximos"/><br>

        <p>Activo:
        <input type=radio  name="habilitar" id="Si"> 
        <label for="Si">Si</label>
        <input type=radio  name="habilitar" id="no">
        <label for="No">No</label><br><br></p>

          <span>Hora de inicio:</span>
  			 <input type="time" id="time" name="hora" min="07:00:00" max="22:00:00"><br><br>           <!--Campo para indicar la hora-->
         <a href="index.html">
         <input type="submit" name="Salir" value="Salir"></a>
         <a href="RegistroEvento.php">
         <input type="submit" name="Enviar" value="Enviar"></a>


              </form>
            </div>
  		 </header>

</body>
</html>