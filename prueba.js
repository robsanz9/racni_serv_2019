function conecTaller() {
	var teiunam=SpreadsheetApp.getActive().getSheetByName('Respuestas del taller');
	return teiunam;
}
function enviaQR(){
	var lock=LockService.getScriptLock();
	var teiunam=conecTaller();
	var datostaller=taller.getDataRange().gerValues();

	for (var i = 1;i<teiunam.getDataRange().getValues().length;i++) {
		var enviado=datostaller[i][8];
		var imagen=[];
		if (enviado!='enviado') {
			var starRow=1+i;
			var hash=datostaller[i][5].slice(0,2)+datostaller[i][3].slice(0,2)+datostaller[i]
			[4].slice(0,2)+i;
			Logger.log(hash);
			var nombre=datostaller[i][2];
			var correo=datostaller[i][5];
			var cuerpo='Bienvenido <b>'+nombre+'</b> al Taller <b> de emprendimiento INNOVAUNAM 2020-1 </b> impartido por la profesora <b>Fabiola Lara </b> en este correo encontraras un código qr, el cual deberás presentar para tomar asistencia.<br><br> '+'
			'Puedes presentar el QR digital o impreso.'+
          '<center><img src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl='+hash+'"><br>Código: <b>'+hash+'</b></center>';

          teiunam.getRange(startRow,9).setValue(hash);
          GmailApp.sendEmail(correo,'Asistencia Taller de Emprendimiento INNOVAUNAM 2020-1', "",{
          	htmlBody: cuerpo})
          teiunam.getRange(startRow,9).setValue('Enviado');

		}
	}
	locke.releaseLock(;)
}